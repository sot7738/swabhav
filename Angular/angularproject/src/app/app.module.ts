import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Comp1Component } from './comp1/comp1.component';
import { BindingComponent } from './binding/binding.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TwowayBindingComponent } from './twoway-binding/twoway-binding.component';
import { StudentComponent } from './student/student.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { FormComponent } from './form/form.component';
import { NoapiComponent } from './noapi/noapi.component';
import { HttpClientModule,HttpClient } from "@angular/common/http";
import { PricecalculatorComponent } from './pricecalculator/pricecalculator.component';
import { PipesComponent } from './pipes/pipes.component';
import { SnakeComponent } from './snake/snake.component';
import { SnakeCasePipe } from './snake/snake-case.pipe';
import { CountryapiComponent } from './countryapi/countryapi.component'
import { StudPipe } from './student/stud.pipe';
import { LogintemplateComponent } from './logintemplate/logintemplate.component';
import { LoginmodeldrivenComponent } from './loginmodeldriven/loginmodeldriven.component';
import { ProductTempComponent } from './product-temp/product-temp.component';
import { StudentapiComponent } from './studentapi/studentapi.component';

@NgModule({
  declarations:[
    AppComponent,
    Comp1Component,
    BindingComponent,
    WelcomeComponent,
    TwowayBindingComponent,
    StudentComponent,
    CalculatorComponent,
    FormComponent,
    NoapiComponent,
    PricecalculatorComponent,
    PipesComponent,
    SnakeComponent,
    SnakeCasePipe,
    CountryapiComponent,
    StudPipe,
    LogintemplateComponent,
    LoginmodeldrivenComponent,
    ProductTempComponent,
    StudentapiComponent
    
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { SnakeCasePipe: any; StudPipe:any}
