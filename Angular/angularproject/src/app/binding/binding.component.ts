import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.scss']
})
export class BindingComponent implements OnInit {

  name: String = "Shobhit";
  constructor() { }

  ngOnInit(): void {
  }
  
  show() {
    console.log(this.name)
  }

}
