import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent  {
  fno:number;
  sno: number;
  message:string ;
  
  add(){
    
    this.message = "addition = "+((this.fno)+(this.sno));
  }

  sub(){
    
    this.message = "subtraction = "+((this.fno)-(this.sno));
  }

  mul(){
    
    this.message = "multiplication = "+((this.fno)*(this.sno));
  }

  div(){
    
    this.message = "Division = "+((this.fno)/(this.sno));
  }

  
}
