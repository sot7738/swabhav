import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryapiComponent } from './countryapi.component';

describe('CountryapiComponent', () => {
  let component: CountryapiComponent;
  let fixture: ComponentFixture<CountryapiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryapiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
