import { Component, OnInit } from '@angular/core';
import { IcountryApi } from './service/IcountryApi';
import { countryService } from './service/countryService';

@Component({
  selector: 'app-countryapi',
  templateUrl: './countryapi.component.html',
  styleUrls: ['./countryapi.component.scss']
})

export class CountryapiComponent {
  loa: IcountryApi[] = [];
  constructor(private _service: countryService) {
  }

  load() {
    this._service.load1().subscribe((data)=>{
      this.loa=data
      console.log(data);
    })
}
}
