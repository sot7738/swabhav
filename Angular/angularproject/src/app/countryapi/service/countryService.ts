import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { IcountryApi } from './IcountryApi';

import { Observable } from 'rxjs'; @Injectable({
  providedIn: "root"
})

export class countryService {
  constructor(private _http: HttpClient) {
  }
  url = "http://restcountries.eu/rest/v2/all";

  load1(): Observable<IcountryApi[]> {
    return this._http.get<IcountryApi[]>(this.url);
  }
}