import { Component, OnInit } from '@angular/core';
import { Iproduct } from './Iproducts';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor() { }

  name:string="";
  products:Iproduct[]=[];
  rating:number;
  ratings:number[]= [1,2,3,4,5];
  ngOnInit(): void {
  }

  addp():void{
    this.products.push({name:this.name,rating:this.rating});
  }

}
