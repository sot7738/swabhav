import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoapiComponent } from './noapi.component';

describe('NoapiComponent', () => {
  let component: NoapiComponent;
  let fixture: ComponentFixture<NoapiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoapiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
