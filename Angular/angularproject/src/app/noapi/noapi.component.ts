import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InumberApi } from './service/InumberApi';

@Component({
  selector: 'app-noapi',
  templateUrl: './noapi.component.html',
  styleUrls: ['./noapi.component.scss']
})

export class NoapiComponent implements OnInit {
  num: number;
  fact: any;
  num1:any;
  _error:any;
  error:any;
  text:any;
  constructor(private _http: HttpClient) {
  }

  ngOnInit(): void {
  }

  url ="http://numbersapi.com/" ;
  inumber:InumberApi;
  data:any;
  numapi() {
    var a=this._http.get<any>(`${this.url}`+`${this.num}`);
         return a.subscribe((data:any)=>{
          this.num1=data;
          console.log(this.num1)
        },((_error: any) => {
          this.fact=_error.error.text;
          console.log(this.fact);
       }));
    }
  }


