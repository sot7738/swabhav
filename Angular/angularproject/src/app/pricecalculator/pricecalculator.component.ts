import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricecalculator',
  templateUrl: './pricecalculator.component.html',
  styleUrls: ['./pricecalculator.component.scss']
})
export class PricecalculatorComponent implements OnInit {
  p:string = "";
  r:string = "";
  cost:number = 0;
  discountAmount:number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  pc(){
    this.discountAmount = (parseInt(this.r)/100 *parseInt(this.p))
    this.cost = parseInt(this.p) - this.discountAmount;
    console.log(this.p);

  }
}
