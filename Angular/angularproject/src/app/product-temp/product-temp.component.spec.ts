import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTempComponent } from './product-temp.component';

describe('ProductTempComponent', () => {
  let component: ProductTempComponent;
  let fixture: ComponentFixture<ProductTempComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductTempComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
