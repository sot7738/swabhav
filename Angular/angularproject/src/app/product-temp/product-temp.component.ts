import { Component, OnInit } from '@angular/core';
import { Iproduct } from './Iproduct';

@Component({
  selector: 'app-product-temp',
  templateUrl: './product-temp.component.html',
  styleUrls: ['./product-temp.component.scss']
})
export class ProductTempComponent {

  products:Iproduct[]=[];

  addp(productName:HTMLInputElement,rating1:HTMLInputElement):void{
    console.log(rating1);
    console.log(productName.value);
    console.log(Iproduct);
    this.products.push({ name : productName.value , rating : parseInt(rating1.value)});
    productName.value="";
  }

}
