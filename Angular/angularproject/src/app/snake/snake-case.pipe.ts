import { Pipe , PipeTransform } from '@angular/core';

@Pipe({
    name: 'snakeCase'
})
export class SnakeCasePipe implements PipeTransform{
    transform(value:string , limit:number): string {
        var words:string = value;//.split(' ');
        var newValue:string="";
        console.log(words.length);
        for(var i=0;i<words.length;i++){
            console.log(i)
            newValue +=words.charAt(i);

            if((i+1) %5==0 && i!=0){
                newValue+="_";  
            }
        }
        // if(words.length <= limit){
        //     newValue = words.concat('_');
        // }
        // else{
        //     words=words.slice(0,5);
        //     newValue=words.concat('_');
        // }
        return newValue;
    }
}