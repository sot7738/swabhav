export interface interfaceStudent {
    rollno: number;
    name: string;
    cgpa: number;
}