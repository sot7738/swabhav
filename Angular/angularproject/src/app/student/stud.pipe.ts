import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'studpipe'
})
export class StudPipe implements PipeTransform {
    transform(cgpa: number) {
        if (cgpa <= 7){
            return "C";
        }
        if (cgpa <= 8 && cgpa > 7){
            return "B";

        }
        if (cgpa > 8){
            return "A";
        }

        return "D";
    }


}