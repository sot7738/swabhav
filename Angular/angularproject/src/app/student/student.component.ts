import { Component, OnInit } from '@angular/core';
import { interfaceStudent } from './interfaceStudent';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent {
student!: interfaceStudent;
  students: interfaceStudent[] = [];
  constructor() { }

  addStudents() {
    this.students = [
      { rollno: 1, name: "Naruto", cgpa: 8.4 },
      { rollno: 2, name: "Luffy", cgpa: 7.2 },
      { rollno: 3, name: "Natsu", cgpa: 6.4 },
      { rollno: 4, name: "Heisnberg", cgpa: 7.7 }
    ]
  }
  addStudent() {
    this.student = { rollno: 79, name: "Shobhit", cgpa: 7.13 };
  }


  ngOnInit(): void {
  }

}
