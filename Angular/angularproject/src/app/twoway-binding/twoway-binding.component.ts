import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-twoway-binding',
  templateUrl: './twoway-binding.component.html',
  styleUrls: ['./twoway-binding.component.scss']
})
 export class TwowayBindingComponent {
  firstName:String
  lastName:String
  constructor() {
    this.firstName = "Shobhit"
    this.lastName = "Tiwari"
   }

   onChange(event:any){
     console.log(event)
     this.firstName = event;
   }

  ngOnInit(): void {
  }

}
