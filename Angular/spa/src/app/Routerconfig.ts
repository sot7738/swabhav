import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AddproductComponent } from './addproduct/addproduct.component';

export const routesArray : Routes = [
    {
        path :'home',
        component : HomeComponent
    },
    {
        path : 'about',
        component : AboutComponent
    },{
        path : 'contactus',
        component : ContactusComponent
    },{
        path : 'addproduct',
        component : AddproductComponent
    },
    {
        path : '',
        redirectTo : 'addproduct',
        pathMatch : 'full'
    },
    {
        path:'**',
        component: NotfoundComponent

    }
];
