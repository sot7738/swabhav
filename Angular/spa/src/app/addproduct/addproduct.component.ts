import { Component, OnInit } from '@angular/core';
import { Iproduct } from '../service/Iproduct';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddproductComponent implements OnInit {

  products:Iproduct[]=[];
  constructor(private localStorageService: LocalStorageService) {
    var products = this.localStorageService.getItem('products');
    this.products=JSON.parse(products);
  }
  ngOnInit(): void {
   
  }

  addp(productName:HTMLInputElement,rating1:HTMLInputElement):void{
    
    console.log(this.products);
    this.products.push({ name : productName.value , rating : parseInt(rating1.value)});
    productName.value="";
    this.localStorageService.setItem('products',this.products);

  }c

}
