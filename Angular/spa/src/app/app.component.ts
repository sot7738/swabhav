import { Component, OnInit } from '@angular/core';
import { Iproduct } from './service/Iproduct';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  user: any;
  dataSource: any;
  length:any;
  products:Iproduct[]=[]; 

  ngOnInit(): void {
    this.dataSource=this.products;
    localStorage.setItem('dataSource', this.dataSource);
    console.log(localStorage.getItem('dataSource'));
  }
  title = 'spa';
}
