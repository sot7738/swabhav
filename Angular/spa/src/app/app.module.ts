import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routesArray } from './Routerconfig';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactusComponent } from './contactus/contactus.component';
import { RouterModule } from '@angular/router';
import {NotfoundComponent} from './notfound/notfound.component';
import { AddproductComponent } from './addproduct/addproduct.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactusComponent,
    NotfoundComponent,
    AddproductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routesArray)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
