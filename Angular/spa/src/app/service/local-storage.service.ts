import { Injectable } from '@angular/core';
import { Iproduct } from './Iproduct';
@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
  constructor() { }
  public setItem(key: string, value: Iproduct[]) {
    localStorage.setItem(key, JSON.stringify(value));
  }
    
  public getItem(key: string){ 
    return localStorage.getItem(key)
  }
  public removeItem(key:string) {
    localStorage.removeItem(key);
  }
  public clear(){
    localStorage.clear(); 
  }
}