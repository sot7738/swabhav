import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AddComponent } from './add/add.component';

export const routesArray : Routes = [
    {
        path :'home',
        component : HomeComponent
    },
   {
        path : 'add',
        component : AddComponent
    },
    {
        path : '',
        redirectTo : 'add',
        pathMatch : 'full'
    },
    {
        path:'**',
        component: NotfoundComponent
    }
];