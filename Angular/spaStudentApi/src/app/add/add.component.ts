import { Component, OnInit } from '@angular/core';
import { IstudentApi } from '../service/IstudentApi';
import { studentapiService } from '../service/studentapi.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  id: any;
  rollNo: any;
  name: any;
  age: any;
  address: any;
  email: any;
  isMale: any;
  date: any;
  students: IstudentApi[] = [];

  constructor(private service: studentapiService) { }

  ngOnInit(): void {
  }

  add1() {
    this.service.add(this.rollNo, this.name, this.age, this.address, this.email,
      this.isMale, this.date)
      .then(r => {
        alert("Student is Added");

        this.ngOnInit();

      })
      .catch(r => { console.log(r) });

  }

}
