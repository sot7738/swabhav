import { Component, OnInit } from '@angular/core';
import { IstudentApi } from '../service/IstudentApi';
import { studentapiService } from '../service/studentapi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  id: any;
  rollNo: any;
  name: any;
  age: any;
  address: any;
  email: any;
  isMale: any;
  date: any;
  constructor(private service: studentapiService) { }
  students: IstudentApi[] = [];

  ngOnInit(): void {
    this.service.load1().subscribe((data) => { this.students = data })
    console.log(this.students)
  }

  delete1(id: any) {
    this.service.delete(id)
      .then(r => {
        alert("Student is Deleted");
        this.ngOnInit();

      })
      .catch(r => { console.log(r) });
  }

  edit1() {
    this.service.edit(this.id, this.rollNo, this.name, this.age, this.address, this.email, 
      this.isMale, this.date)
      .then(r => {
        alert("Student Data is edited");

      })
      .catch(r => console.log(r));
  }

}
