import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { IstudentApi } from './IstudentApi';
import { Observable } from 'rxjs'; 

@Injectable({
  providedIn: "root"
})

export class studentapiService {
  id: any;
  rollNo: any;
  name: any;
  age: any;
  address: any;
  email: any;
  isMale: any;
  date: any;
  editSuccess: any;
  constructor(private _http: HttpClient) {
  }
  url = "http://gsmktg.azurewebsites.net/api/v1/techlabs/test/students/";

  load1(): Observable<IstudentApi[]> {
    return this._http.get<IstudentApi[]>(this.url);
  }

  // add(){
  //   this._http.post(this.url, {id:115, rollNo: 79, name: "Shobhit Tiwari",age:24,
  //   address:"kalyan",email:"sot7738@gmail.com", isMale: true})
  //   .subscribe((data: any) => {
  //       console.log(data)
  //   }, (error) => {
  //       console.log(error)
  //   })

  // }

  add(rollNo: any,name: any,age: any,address: any,email: any,isMale: any,date: any){
    let addStudent = {
      rollNo:rollNo,
      name:name,
      age:age,
      address:address,
      email:email,
      isMale:isMale,
      date:date,
    }

    return this._http.post(this.url,addStudent).toPromise();
}

  delete(id: any) {
    return this._http.delete(this.url + id).toPromise();
  }

  edit(id: any,rollNo: any,name: any,age: any,address: any,email: any,isMale: any,date: any){
    let editStudent = {
      id:id,
      rollNo:rollNo,
      name:name,
      age:age,
      address:address,
      email:email,
      isMale:isMale,
      date:date,
    }
    var headers = new Headers();
    headers.append('Authorization', 'Bearer AADDFFKKKLLLL');

    return this._http.put(this.url,editStudent).toPromise();
}
}