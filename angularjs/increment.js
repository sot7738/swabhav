angular.module("incr", [])
    .controller("IncrementController", function ($scope, $rootScope, CounterFactory) {
        $rootScope.count = 0;
        $scope.increment = function () {
            $rootScope.count = CounterFactory.inc($rootScope.count);
        }
    })
    .controller("DecrementController", function ($scope, $rootScope, CounterFactory) {
        $rootScope.count = 0;
        $scope.decrement = function () {
            $rootScope.count = CounterFactory.dec($rootScope.count);
        }
    })
    .factory("CounterFactory", function () {
        var CounterFactory = {};
        CounterFactory.inc = function (count) {
            return count + 1;
        }
        CounterFactory.dec = function (count) {
            return count - 1;
        }
        return CounterFactory;
    })