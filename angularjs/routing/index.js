angular.module("RouteApp", ["ngRoute"]).config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "resource/home.html",
                controller: "HomeController"
            })
            .when("/home", {
                templateUrl: "resource/home.html",
                controller: "HomeController"
            })
            .when("/about", {
                templateUrl: "resource/about.html",
                controller: "AboutController"
            })
            .when("/career", {
                templateUrl: "resource/career.html",
                controller: "CareerController"
            })
    }])

    .controller("HomeController", ["$scope", function ($scope) {
        $scope.data = "we are at Home Screen";
        console.log($scope.data)
    }])
    .controller("AboutController", ["$scope", function ($scope) {
        $scope.data = "we are at about Screen";
        console.log($scope.data)

    }])
    .controller("CareerController", ["$scope", function ($scope) {
        $scope.data = "we are at career Screen";
        console.log($scope.data)

    }])
