import { Routes } from "@angular/router";
import { NotfoundComponent } from "./notfound/notfound.component";
import { ProductDetailComponentComponent } from "./product-detail-component/product-detail-component.component";
import { ProductListComponentComponent } from "./product-list-component/product-list-component.component";
import { RouteGuard } from "./router-guard";
import { WelcomeComponentComponent } from "./welcome-component/welcome-component.component";

export const ROUTES_ARRAY: Routes = [
    { path: 'welcome', component: WelcomeComponentComponent },
    { path: 'productList', component: ProductListComponentComponent },
    { path: 'productDetail/:id', component: ProductDetailComponentComponent , canActivate:[RouteGuard] },
    { path: 'productDetail', component: ProductDetailComponentComponent },
    { path: '', component: WelcomeComponentComponent, pathMatch: 'full' },
    { path: '**',  component: NotfoundComponent }
]