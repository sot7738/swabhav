import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pipe } from 'rxjs';
import { filter, map } from "rxjs/operators";

@Injectable()
export class ProductDataService {
    id: any;
    productId: any;
    constructor(private http: HttpClient) { }

    public getJsonData() {
        console.log("http was fired");
        return this.http.get("assets/products.json");
    }

    public GetDataById(id: any) {
        console.log(id);
        return this.http.get("assets/products.json")
    }
}
