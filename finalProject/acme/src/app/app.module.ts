import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { HttpClientModule,HttpClient } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponentComponent } from './welcome-component/welcome-component.component';
import { ProductListComponentComponent } from './product-list-component/product-list-component.component';
import { StarComponentComponent } from './star-component/star-component.component';
import { ProductDetailComponentComponent } from './product-detail-component/product-detail-component.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ROUTES_ARRAY } from './RouteConfig';
import { RouterModule } from '@angular/router';
import { ProductDataService } from './Service/ProductData.service';
import { RouteGuard } from './router-guard';
import { RatingModule } from 'ng-starrating';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponentComponent,
    ProductListComponentComponent,
    StarComponentComponent,
    ProductDetailComponentComponent,
    NotfoundComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RatingModule,
    RouterModule.forRoot(ROUTES_ARRAY)
  ],
  providers: [ProductDataService,RouteGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
