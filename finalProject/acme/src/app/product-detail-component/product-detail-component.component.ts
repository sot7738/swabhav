import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductDataService } from '../Service/ProductData.service';
import { Router } from '@angular/router';
import { RouteGuard } from '../router-guard';

@Component({
    selector: 'app-product-detail-component',
    templateUrl: './product-detail-component.component.html',
    styleUrls: ['./product-detail-component.component.scss']
})
export class ProductDetailComponentComponent implements OnInit {
    FLAG: any;

    ngOnInit(): void {
    }
    id: any;
    data: any;

    constructor(public auth:RouteGuard,private route: ActivatedRoute, private getService: ProductDataService) {
        this.route.params.subscribe(params => { this.id = params['id']; })
        console.log("id:" + this.id);
        this.GetDetails();
    }

    GetDetails() {
        var data = localStorage.getItem("products");
        if (data == undefined) {
            this.getService.getJsonData()
                .subscribe((r) => {
                    localStorage.setItem("products", JSON.stringify(r));
                    this.data = (r as any[]).find((d) => this.id == d.productId);
                })
        }
        else {
            this.data = (JSON.parse(data) as any[]).find((d) => this.id == d.productId);
        }
    }
    GetDetail(id: any) {
        this.getService.GetDataById(this.id)
        console.log("Inside GetDetails..." + id);
    }
}
