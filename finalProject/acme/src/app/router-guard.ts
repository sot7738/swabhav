import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { ProductDataService } from './Service/ProductData.service';

@Injectable({
    providedIn: 'root'
})

export class RouteGuard implements CanActivate {
    data: any = [];
    constructor(private service:ProductDataService,private router:Router){
    }
    canActivate(route:ActivatedRouteSnapshot): Observable<any> {
        let id= route.params.id;
        return this.service.getJsonData().pipe(map(r => {
            this.data = r;
            console.log("Inside Route Guard")
            for(let d of this.data){
                if(id==d.productId){
                    return true;
                }
            }
            this.router.navigate(['/productList'])
            alert("Wrong Product Id")
            return false;

        }));


    }
    
}