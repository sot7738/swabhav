import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-star-component,[app-star-component]',
  templateUrl: './star-component.component.html',
  styleUrls: ['./star-component.component.scss']
})
export class StarComponentComponent  {
  constructor() { }

  @Input()
    rating:any;
    @Output()
    clickRating: EventEmitter<number> = new EventEmitter<number>();
     
   LogRating(){
       //console.log(this.rating);
       this.clickRating.emit(this.rating);
   } 

}
