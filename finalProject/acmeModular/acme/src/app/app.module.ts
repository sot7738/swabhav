import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule,HttpClient } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponentComponent } from './welcome-component/welcome-component.component';
import { ROUTES_ARRAY } from './RouteConfig';
import { RouterModule } from '@angular/router';
import { ProductDataService } from './Service/ProductData.service';
import { RouteGuard } from './router-guard';
import { RatingModule } from 'ng-starrating';
import { ProductModuleComponent } from './product-module/product-module.component';
import { SharedModuleComponent } from './shared-module/shared-module.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RatingModule,
    ProductModuleComponent,
    SharedModuleComponent,
    RouterModule.forRoot(ROUTES_ARRAY)
  ],
  providers: [ProductDataService,RouteGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
