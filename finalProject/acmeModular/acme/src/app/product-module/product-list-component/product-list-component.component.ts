import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductDataService } from '../../Service/ProductData.service';
import { HttpClientModule } from '@angular/common/http'

@Component({
    selector: 'app-product-list-component',
    templateUrl: './product-list-component.component.html',
    styleUrls: ['./product-list-component.component.scss']
})
export class ProductListComponentComponent implements OnInit {

    data: any = [];
    FLAG = 0;
    id: any;
    totalData: any = [];
    filterdata = "";
    btnvalue: any;

    constructor(private productService: ProductDataService, private router: Router) {
        this.btnvalue = 'Show Image';
    }

    ngOnInit() {
        var data = localStorage.getItem("products");
        if (data == undefined) {
            this.productService.getJsonData()
                .subscribe((r: any) => {
                    localStorage.setItem("products", JSON.stringify(r));
                    this.data = r;
                    this.totalData = r;
                });
        }
        else {
            this.data = JSON.parse(data);
            this.totalData = JSON.parse(data);
        }
    }

    onfilter(e: any) {
        console.log(e);
        if (e) {
            this.data = this.totalData.filter((d: { productName: any; }) => d.productName.includes(e))
        }
        else {
            this.data = this.totalData;
        }
    }

    toggleImage() {
        console.log(this.FLAG)
        if (this.FLAG == 0) {

            this.FLAG = 1;
            this.btnvalue = 'Hide Image';
        }
        else {
            this.FLAG = 0;
            this.btnvalue = 'Show Image';
        }
    }

    GetDetail(id: any) {
        this.productService.GetDataById(this.id)
        console.log("Inside GetDetails..." + id);
        this.router.navigate(['productDetail', id]);
    }


}
