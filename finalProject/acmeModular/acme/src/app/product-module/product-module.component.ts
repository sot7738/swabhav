import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppModule  } from '../app.module';
import { ProductDetailComponentComponent } from './product-detail-component/product-detail-component.component';
import { ProductListComponentComponent } from './product-list-component/product-list-component.component';
import { SharedModuleComponent } from '../shared-module/shared-module.component';

@NgModule({
  declarations: [
    ProductDetailComponentComponent,
    ProductListComponentComponent
  ],
  imports: [
    RouterModule,
    SharedModuleComponent
  ],
  
})
export class ProductModuleComponent { }
