import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppModule  } from '../app.module';
import { StarComponentComponent } from './star-component/star-component.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'

@NgModule({
  declarations: [
    StarComponentComponent,
  ],
  imports: [
    RouterModule
  ],

  exports:[
    StarComponentComponent,
    FormsModule,
    CommonModule
  ]
  
})
export class SharedModuleComponent { }
