import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-star-component,[app-star-component]',
  templateUrl: './star-component.component.html',
  styleUrls: ['./star-component.component.scss']
})
export class StarComponentComponent  {
  constructor() { }

  @Input()
    rating:any;
    @Output()
     
   LogRating(){
       console.log(this.rating);
   } 

}
