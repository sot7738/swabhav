var Person = /** @class */ (function () {
    function Person(name, id, age, height, weight) {
        this.Name = name;
        this.Id = id;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }
    return Person;
}());
var p = new Person('Shobhit', 21, 24, 180, 90);
console.log(p);
