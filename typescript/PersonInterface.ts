interface Person {
    name : string;
    id:number;
    age:number;
    height:number;
    weight:number;
}

function displayDetails(person:Person){
    console.log(person);
}
let personObject = {
    name: 'Shobhit Tiwari',
    id: 79,
    age:24,
    height:180,
    weight:90
}
displayDetails(personObject);
