class Employee {
    employeeName: string;

    constructor(name: string){
        this.employeeName=name;
    }

    greet() {
        console.log(`Good Morning ${this.employeeName}`)
    }
}
let emp = new Employee('Shobhit');
console.log(emp.employeeName);
emp.greet();