"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address1 = exports.Employee = void 0;
var Employee = /** @class */ (function () {
    function Employee(Name, Id) {
        this.Name = Name;
        this.Id = Id;
    }
    Object.defineProperty(Employee.prototype, "id", {
        get: function () {
            return this.Id;
        },
        set: function (value) {
            this.Id = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Employee.prototype, "name", {
        get: function () {
            return this.Name;
        },
        set: function (value) {
            this.Name = value;
        },
        enumerable: false,
        configurable: true
    });
    return Employee;
}());
exports.Employee = Employee;
var Address1 = /** @class */ (function () {
    function Address1(address) {
        this.address = address;
    }
    Object.defineProperty(Address1.prototype, "Address", {
        get: function () {
            return this.address;
        },
        enumerable: false,
        configurable: true
    });
    return Address1;
}());
exports.Address1 = Address1;
