export class Employee {
    
    constructor(private Name: String, private Id: number) { }
    public get id(): number {
        return this.Id;
    }
    public get name(): String {
        return this.Name;
    }
    public set id(value: number) {
        this.Id = value;
    }
    public set name(value: String) {
        this.Name = value;
    }

}

export class Address1{
    constructor (private address:String){}
    get Address(){
        return this.address
        }
}