interface Person {
     firstName:String;
     lastName:String;
}

function personDetails(person:Person){
    console.log(`${person.firstName} ${person.lastName}`);
}

let p = {
    firstName: 'Natsu',
    lastName: 'Dragneel'
}

personDetails(p);